/* This file is part of Smap.
   Copyright (C) 2006-2021 Sergey Poznyakoff

   Smap is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Smap is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Smap.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <stdlib.h>
#include <string.h>
#include "common.h"
#include "smap/diag.h"

void
emalloc_die(size_t size)
{
	smap_error("out of memory (allocating %lu bytes)",
		   (unsigned long) size);
	abort();
}

void *
emalloc(size_t size)
{
	void *p = malloc(size);
	if (!p)
		emalloc_die(size);
	return p;
}

void *
ecalloc(size_t nmemb, size_t size)
{
	void *p = calloc(nmemb, size);
	if (!p)
		emalloc_die(size);
	return p;
}

void *
erealloc(void *ptr, size_t newsize)
{
	void *p = realloc(ptr, newsize);
	if (!p)
		emalloc_die(newsize);
	return p;
}

char *
estrdup(const char *s)
{
	return strcpy(emalloc(strlen(s) + 1), s);
}

void *
e2nrealloc(void *p, size_t *pn, size_t s)
{
	size_t n = *pn;
	char *newp;
	
	if (!p) {
		if (!n) {
			/* The approximate size to use for initial small
			   allocation requests, when the invoking code
			   specifies an old size of zero.  64 bytes is
			   the largest "small" request for the
			   GNU C library malloc.  */
			enum { DEFAULT_MXFAST = 64 };
			
			n = DEFAULT_MXFAST / s;
			n += !n;
		}
	} else {
		/* Set N = ceil (1.5 * N) so that progress is made if N == 1.
		   Check for overflow, so that N * S stays in size_t range.
		   The check is slightly conservative, but an exact check isn't
		   worth the trouble.  */
		if ((size_t) -1 / 3 * 2 / s <= n) {
			emalloc_die(n * s);
		}
		n += (n + 1) / 2;
	}

	newp = erealloc(p, n * s);
	*pn = n;
	return newp;
}
