/* This file is part of Smap.
   Copyright (C) 2006-2021 Sergey Poznyakoff

   Smap is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Smap is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Smap.  If not, see <http://www.gnu.org/licenses/>. */

#include "smapd.h"
#include <dlfcn.h>

struct search_path {
	char **pathv;
	size_t pathc;
	size_t pathn;
};

struct search_path mod_load_path[2];

void
add_load_path(const char *path, int pathid)
{
	struct search_path *sp = &mod_load_path[pathid];
	size_t i;
	struct wordsplit ws;

	ws.ws_delim = ":";
	ws.ws_error = smap_error;
	if (wordsplit(path, &ws,
		      WRDSF_NOCMD | WRDSF_NOVAR |
		      WRDSF_ENOMEMABRT |
		      WRDSF_DELIM | WRDSF_ERROR)) {
		smap_error("cannot parse load path: %s",
			   wordsplit_strerror(&ws));
		abort();
	}

	for (i = 0; i < ws.ws_wordc; i++) {
		if (sp->pathc == sp->pathn)
			sp->pathv = e2nrealloc(sp->pathv, &sp->pathn,
					       sizeof(sp->pathv[0]));
		sp->pathv[sp->pathc++] = ws.ws_wordv[i];
	}
	ws.ws_wordc = 0;
	
	wordsplit_free(&ws);
}

void
smap_modules_init(void)
{
}


struct list_head module_head = LIST_HEAD_INITIALIZER(module_head);

static struct smap_module_instance *
module_locate(char const *id)
{
	struct smap_module_instance *p;
	LIST_FOREACH(p, &module_head, link) {
		if (strcmp(p->id, id) == 0)
			return p;
	}
	return NULL;
}

static inline void
module_attach(struct smap_module_instance *inst)
{
	LIST_HEAD_INSERT_LAST(&module_head, inst, link);
}

static inline void
module_detach(struct smap_module_instance *inst)
{
	LIST_REMOVE(inst, link);
}

void remove_dependent_databases(const char *id);

int
module_declare(const char *file, unsigned line,
	       const char *id, int argc, char **argv,
	       struct smap_module_instance **pmod)
{
	int i;
	struct smap_module_instance *mip;

	mip = module_locate(id);
	if (mip) {
		*pmod = mip;
		return 1;
	}

	mip = emalloc(sizeof(*mip));
	mip->file = estrdup(file);
	mip->line = line;
	mip->id = estrdup(id);
	mip->argc = argc;
	mip->argv = ecalloc(argc + 1, sizeof(mip->argv[0]));
	for (i = 0; i < argc; i++)
		mip->argv[i] = estrdup(argv[i]);
	mip->argv[i] = NULL;
	mip->module = NULL;
	mip->handle = NULL;
	list_head_init(&mip->link);
	module_attach(mip);
	*pmod = mip;
	return 0;
}

static void *
try_load_in_path(char const *file, char **pathv, size_t pathc)
{
	void *handle = NULL;
	size_t i;
	char *namebuf = NULL;
	size_t namelen = 0;

	size_t flen = strlen(file);
	
	static char const libdir[] = ".libs";
	static size_t libdirlen = sizeof(libdir)-1;

	for (i = 0; i < pathc; i++) {
		size_t plen = strlen(pathv[i]);
		size_t len;
		
		if (plen == 0)
			continue;
		if (pathv[i][plen-1] == '/')
			plen--;

		len = plen + flen + 2;
		if (len > namelen) {
			namebuf = erealloc(namebuf, len);
			namelen = len;
		}
		memcpy(namebuf, pathv[i], plen);
		namebuf[plen] = '/';
		strcpy(namebuf + plen + 1, file);

 		handle = dlopen(namebuf, RTLD_NOW | RTLD_GLOBAL);
		if (handle)
			break;

		/* Append .libs */
		if (!(plen > libdirlen + 1
		      && pathv[i][plen - libdirlen - 1] == '/'
		      && memcmp(pathv[i] + plen - libdirlen, libdir, libdirlen) == 0)) {
			len += libdirlen + 1;

			if (len > namelen) {
				namebuf = erealloc(namebuf, len);
				namelen = len;
			}

			strcpy(namebuf + plen + 1, libdir);
			strcat(namebuf, "/");
			strcat(namebuf, file);

			handle = dlopen(namebuf, RTLD_NOW | RTLD_GLOBAL);
			if (handle)
				break;
		}
	}
	free(namebuf);
	return handle;
}

#define MODULE_ASSERT(cond)						\
	do {								\
		if (!(cond)) {						\
			dlclose(handle);				\
			smap_error("%s: faulty module: (%s) failed",	\
				   inst->id,				\
				   #cond);				\
			return 1;					\
		}							\
	} while (0)

static int
_load_module(struct smap_module_instance *inst)
{
	void *handle;
	struct smap_module *pmod;
	static char suf[] = ".so";
	static char suflen = sizeof(suf) - 1;
	size_t len;
	char *libtmp = NULL;
	char const *libname;
	
	debug(DBG_MODULE, 2, ("loading module %s", inst->id));

	if (inst->handle) {
		smap_error("module %s already loaded", inst->id);
		return 1;
	}

	len = strlen(inst->argv[0]);
	if (len > suflen
	    && memcmp(inst->argv[0] + len - suflen, suf, suflen) == 0)
		libname = inst->argv[0];
	else {
		libtmp = emalloc(len + suflen + 1);
		libname = strcat(strcpy(libtmp, inst->argv[0]), suf);
	}
	
	handle = try_load_in_path(libname, mod_load_path[PATH_PREPEND].pathv,
				  mod_load_path[PATH_PREPEND].pathc);
	if (handle == NULL) {
		char *path = getenv("LD_LIBRARY_PATH");
		if (path) {
			struct wordsplit ws;
			ws.ws_delim = ":";
			ws.ws_error = smap_error;

			if (wordsplit(path, &ws,
				      WRDSF_NOCMD | WRDSF_NOVAR |
				      WRDSF_ENOMEMABRT |
				      WRDSF_DELIM | WRDSF_ERROR)) {
				smap_error("cannot parse load path: %s",
					   wordsplit_strerror(&ws));
				abort();
			}

			handle = try_load_in_path(libname,
						  ws.ws_wordv, ws.ws_wordc);
			wordsplit_free(&ws);
		}

		if (handle == NULL) {
			char *moddir = SMAP_MODDIR;
			handle = try_load_in_path(libname, &moddir, 1);
		}

		if (handle == NULL) {
			try_load_in_path(libname,
					 mod_load_path[PATH_APPEND].pathv,
					 mod_load_path[PATH_APPEND].pathc);
		}
	}
	free(libtmp);
	
	if (!handle) {
		smap_error("cannot load module %s: %s", inst->id, dlerror());
		return 1;
	}

	pmod = (struct smap_module *) dlsym(handle, "smap_module");
	MODULE_ASSERT(pmod != NULL);
	MODULE_ASSERT(pmod->smap_version <= SMAP_MODULE_VERSION);
	MODULE_ASSERT(pmod->smap_init_db);
	MODULE_ASSERT(pmod->smap_free_db);
	if (pmod->smap_version == 1)
	    	MODULE_ASSERT(pmod->smap_query);
	else {
		if (pmod->smap_capabilities & SMAP_CAPA_QUERY)
			MODULE_ASSERT(pmod->smap_query);
		if (pmod->smap_capabilities & SMAP_CAPA_XFORM)
			MODULE_ASSERT(pmod->smap_xform);
		if (pmod->smap_capabilities & SMAP_CAPA_DBCAP)
			MODULE_ASSERT(pmod->smap_dbcap);
	}
	
	if (pmod->smap_init && pmod->smap_init(inst->argc, inst->argv)) {
		dlclose(handle);
		smap_error("%s: initialization failed", inst->argv[0]);
		return 1;
	}
	inst->handle = handle;
	inst->module = pmod;
	return 0;
}

void
module_instance_free(struct smap_module_instance *inst)
{
	int i;
	free(inst->file);
	free(inst->id);
	for (i = 0; i < inst->argc; i++)
		free(inst->argv[i]);
	free(inst->argv);
	/* FIXME: Handle */
	free(inst);
}

void
smap_modules_load()
{
	struct smap_module_instance *p, *tmp;

	debug(DBG_MODULE, 1, ("loading modules"));
	LIST_FOREACH_SAFE(p, tmp, &module_head, link) {
		if (_load_module(p)) {
			remove_dependent_databases(p->id);
			debug(DBG_MODULE, 2, ("removing module %s", p->id));
			module_detach(p);
			module_instance_free(p);
		}
	}
}

void
smap_modules_unload()
{
	struct smap_module_instance *p;

	LIST_FOREACH(p, &module_head, link) {
		debug(DBG_MODULE, 2, ("unloading module %s", p->id));
		dlclose(p->handle);
		p->handle = NULL;
	}
}

struct list_head database_head = LIST_HEAD_INITIALIZER(database_head);

struct smap_database_instance *
database_locate(char const *id)
{
	struct smap_database_instance *p;
	LIST_FOREACH(p, &database_head, link) {
		if (strcmp(p->id, id) == 0)
			return p;
	}
	return NULL;
}

static inline void
database_attach(struct smap_database_instance *inst)
{
	LIST_HEAD_INSERT_LAST(&database_head, inst, link);
}

static inline void
database_detach(struct smap_database_instance *inst)
{
	LIST_REMOVE(inst, link);
}

int
database_declare(const char *file, unsigned line,
		 const char *id, const char *modname, int argc, char **argv,
		 struct smap_database_instance **pdb)
{
	int i;
	struct smap_database_instance *db;

	db = database_locate(id);
	if (db) {
		*pdb = db;
		return 1;
	}
	db = ecalloc(1, sizeof(*db));
	db->file = estrdup(file);
	db->line = line;
	db->id = estrdup(id);
	db->modname = estrdup(modname);
	db->argc = argc;
	db->argv = ecalloc(argc + 1, sizeof(db->argv[0]));
	for (i = 0; i < argc; i++)
		db->argv[i] = estrdup(argv[i]);
	db->argv[i] = NULL;
	db->inst = NULL;
	list_head_init(&db->link);
	database_attach(db);
	*pdb = db;
	return 0;
}

void
database_free(struct smap_database_instance *db)
{
	int i;
	free(db->file);
	free(db->id);
	free(db->modname);
	for (i = 0; i < db->argc; i++)
		free(db->argv[i]);
	free(db->argv);
	free(db);
}

void
remove_dependent_databases(const char *modname)
{
	struct smap_database_instance *p, *tmp;

	LIST_FOREACH_SAFE(p, tmp, &database_head, link) {
		if (strcmp(p->modname, modname) == 0) {
			debug(DBG_MODULE, 1, ("removing database %s", p->id));
			database_detach(p);
			database_free(p);
		}
	}
}

void
init_databases(void)
{
	struct smap_database_instance *p, *tmp;

	debug(DBG_DATABASE, 1, ("initializing databases"));
	LIST_FOREACH_SAFE(p, tmp, &database_head, link) {
		struct smap_module_instance *inst = module_locate(p->modname);

		if (!inst)
			smap_error("%s:%u: module %s is not declared",
				   p->file, p->line, p->modname);
		else {
			debug(DBG_DATABASE, 2, ("initializing database %s",
						inst->id));
			
			p->dbh = inst->module->smap_init_db(p->id,
				                            p->argc,
							    p->argv);
			if (!p->dbh)
				smap_error("%s:%u: module %s: "
					   "database initialization failed",
					   p->file, p->line, p->modname);
		}
		if (p->dbh)
			p->inst = inst;
		else {
			debug(DBG_DATABASE, 2,
			      ("removing database %s", p->id));
			database_detach(p);
			database_free(p);
		} 
	}
}

void
close_databases()
{
	struct smap_database_instance *p;

	debug(DBG_DATABASE, 1, ("closing databases"));
	LIST_FOREACH(p, &database_head, link) {
		if (p->opened) {
			debug(DBG_DATABASE, 2,
			      ("closing database %s", p->id));
			if (p->inst->module->smap_close)
				p->inst->module->smap_close(p->dbh);
			p->opened = 0;
		}
	}
}

void
free_databases()
{
	struct smap_database_instance *p;

	debug(DBG_DATABASE, 1, ("freeing databases"));
	LIST_FOREACH(p, &database_head, link) {
		struct smap_module *mod = p->inst->module;
		debug(DBG_DATABASE, 2,
		      ("freeing database %s", p->id));
		if (mod->smap_free_db)
			mod->smap_free_db(p->dbh);
		p->dbh = NULL;
	}
}
