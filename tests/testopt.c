#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <smap/parseopt.h>
#include <smap/diag.h>

#define M_ONE 0x01
#define M_TWO 0x02

int flags;
int yesno;
char *text;
char *const_text;
int tristate;
long number, constant;
int sel;

static int
func(struct smap_option const *opt, const char *val, char **errmsg)
{
	if (strcmp(val, "yes") == 0)
		tristate = 0;
	else if (strcmp(val, "no") == 0)
		tristate = 1;
	else if (strcmp(val, "never") == 0)
		tristate = 2;
	else {
		*errmsg = "bad value for tristate";
		return 1;
	}
	return 0;
}

static const char *sel_choice[] = { "read", "write", "execute", "stop" };

struct smap_option option[] = {
	{ SMAP_OPTSTR(tristate), smap_opt_null,
	  &tristate, { 0 }, func },
	
	{ SMAP_OPTSTR(bool), smap_opt_bool,
	  &yesno, { M_ONE } },
	
	{ SMAP_OPTSTR(one), smap_opt_bitmask,
	  &flags, { M_ONE } },
	{ SMAP_OPTSTR(two), smap_opt_bitmask,
	  &flags, { M_TWO } },

	{ SMAP_OPTSTR(clear-two), smap_opt_bitmask_rev,
	  &flags, { M_TWO } },
	
	{ SMAP_OPTSTR(text), smap_opt_string,
	  &text },

	{ SMAP_OPTSTR(number), smap_opt_long,
	  &number },

	{ SMAP_OPTSTR(select), smap_opt_enum,
	  &sel, { enumstr: sel_choice } },
	
	{ SMAP_OPTSTR(hundred), smap_opt_const,
	  &constant, { 100 } },
	{ SMAP_OPTSTR(twohundred), smap_opt_const,
	  &constant, { 200 } },

	{ SMAP_OPTSTR(ctext), smap_opt_const_string,
	  &const_text },
	
	{ NULL }
};

int
main(int argc, char **argv)
{
	int pflags = 0;
	int i;
	char *modname = NULL;
	
	smap_set_program_name(argv[0]);
	smap_openlog_stderr(0);

	while ((i = getopt(argc, argv, "m:p")) != EOF) {
		switch (i) {
		case 'm':
			modname = optarg;
			break;
		case 'p':
			pflags |= SMAP_PARSEOPT_PERMUTE;
			break;
		default:
			exit(1);
		}
	}
	
	argc -= optind;
	argv += optind;

	if (modname) {
		*--argv = modname;
		++argc;
	} else
		pflags |= SMAP_PARSEOPT_PARSE_ARGV0;
	
	if (smap_parseopt(option, argc, argv, pflags, &i))
		return 1;

	printf("flags=%#2x\n", flags);
	printf("bool=%d\n", yesno);
	if (text) {
		printf("text=%s\n", text);
		free(text);
	} else
		printf("text unset\n");
	if (const_text)
		printf("const_text=%s\n", const_text);
	else
		printf("const_text unset\n");
	printf("tristate=%d\n", tristate);
	printf("number=%ld\n", number);
	printf("constant=%ld\n", constant);
	printf("selection=%d\n", sel);
	       
	printf("Arguments:\n");
	for (; i < argc; i++)
		printf("  %s\n", argv[i]);
	return 0;
}
