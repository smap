/*
  NAME
    smaptest - smapd/smapc test driver

  SYNOPSIS
    smaptest [-hv] [-c FILE] [-C OPT] [-S OPT] [-t SECONDS]
             [-x COMMAND] MAP KEY [MAP KEY...]

  DESCRIPTION
    Starts the smapd daemon in background, with the command line composed
    from "-c FILE" and any OPTs given with the -S (server) command line
    option.  The port to listen on is selected as the first free port on
    127.0.0.1.  The options '--inetd --foreground --stderr' are passed to
    the server by default.
    
    Once smapd is running, smapc is started with the options
    '-S inet://127.0.0.1:PORT -a -q', where PORT is the selected port number.
    Additional options for smapc can be supplied witht the -C (client)
    command line option.  The rest of arguments (MAP KEY...) is appended to
    the smapc command line.

    If an "extra command" is supplied with the -x option, it is started
    first (before starting smapd).  This can be used to start additional
    software smapd needs to connect to.  Currently it is used in ldap
    module tests.
    
    The binaries are looked up in PATH.  If smapc does not terminate within
    10 seconds, it and two remaining programs will be terminated forcefully. 
    
  OPTIONS
    -c FILE
       Supplies the server configuration file name to use instead of
       the default.

    -C OPT
       Appends OPT to the smapc command line.

    -h
       Display a short command summary.
       
    -S OPT
       Appends OPT to the smapd command line.

    -t SECONDS
       Sets execution timeout.  Default is 10 seconds.

    -v
       Increase message verbosity.
       
    -x COMMAND
       Start shell COMMAND before running smapd.

  EXIT CODE
    On success, the program returns the exit code of the smapc
    program.  Other possible error codes:

    64  Command line usage error
    69  smapd or extra command exited prematurely or smapc terminated
        on a signal, or smaptest was forcefully terminated (e.g. by
	sending it a SIGTERM).
    71  System error (e.g., can't fork)
    75  smapc timed out
    
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <errno.h>
#include <unistd.h>
#include <sysexits.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <assert.h>

char const *progname;

static void
error(int exit_code, int error_code, char const *fmt, ...)
{
	va_list ap;

	fprintf(stderr, "%s: ", progname);
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	if (error_code)
		fprintf(stderr, ": %s", strerror(error_code));
	fputc('\n', stderr);
	if (exit_code)
		exit(exit_code);
}

static char urlbuf[80];

static int
listener_setup(void)
{
	struct addrinfo *ap, hints;
	socklen_t len;
	int fd;
	int i;
	char serv[80];
  
	/* Find first free port */
	memset(&hints, 0, sizeof(hints));
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = 0;
	hints.ai_family = AF_INET;
	
	if ((i = getaddrinfo("127.0.0.1", NULL, &hints, &ap)) != 0)
		error(EX_OSERR, errno, "getaddrinfo: %s", gai_strerror(i));
	
	fd = socket(PF_INET, SOCK_STREAM, 0);
	if (fd < 0)
		error(EX_OSERR, errno, "socket");
	i = 1;
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &i, sizeof(i));
	
	((struct sockaddr_in*)ap->ai_addr)->sin_port = 0;
	if (bind(fd, ap->ai_addr, ap->ai_addrlen) < 0)
		error (EX_OSERR, errno, "bind");

	if (listen(fd, 8) == -1)
		error(EX_OSERR, errno, "listen");

	len = ap->ai_addrlen;
	if (getsockname(fd, ap->ai_addr, &len))
		error(EX_OSERR, errno, "getsockname");

	/* Prepare environment */
	if ((i = getnameinfo(ap->ai_addr, len, NULL, 0, serv, sizeof serv,
			     NI_NUMERICSERV)) != 0)
		error(EX_OSERR, errno, "getnameinfo: %s", gai_strerror(i));

	snprintf(urlbuf, sizeof urlbuf, "inet://127.0.0.1:%s", serv);
	
	return fd;
}

void
usage(FILE *fp)
{
	fprintf(fp, "usage: %s [-hv] [-c FILE] [-C OPT] [-S OPT] [-t N] [-x COMMAND] MAP KEY [MAP KEY...]\n", progname);
}

void
progexited(char const *prog, int status)
{
	if (WIFEXITED(status)) {
		if (WEXITSTATUS(status))
			error(0, 0,
			      "%s gone (exit code %d)",
			      prog, WEXITSTATUS(status));
	} else if (WIFSIGNALED(status))
		error(0, 0,
		      "%s gone (signal %d)",
		      prog, WTERMSIG(status));
	else
		error(0, 0,
		      "%s gone (%d)",
		      prog, status);
}

enum {
	ACTION_CONT,
	ACTION_CLEANUP,
	ACTION_TIMEOUT,
	ACTION_CANCEL
};
static int action = ACTION_CONT;

void
sighan(int sig)
{
	switch (sig) {
	case SIGALRM:
		action = ACTION_TIMEOUT;
		break;

	case SIGCHLD:
		action = ACTION_CLEANUP;
		break;

	default:
		action = ACTION_CANCEL;
	}
}

int
main(int argc, char **argv)
{
	int i;
	int lfd;
	pid_t client_pid = -1, server_pid = -1, extra_pid = -1;
	int result = 0;
	int stop;
	int verbose = 0;
	int timeout = 10;
	
	static char *def_args[] = {
		"--inetd",
		"--foreground",
		"--stderr"
	};
	static int def_argc = sizeof(def_args) / sizeof(def_args[0]);

	static int signum[] = {
		SIGALRM, SIGCHLD, SIGHUP, SIGINT, SIGQUIT, SIGTERM
	};

	char *config_file = NULL;
	char *extra_server = NULL;
	
	int c_argc;
	char **c_argv;
	
	int s_argc;
	char **s_argv;

	progname = argv[0];

	s_argv = calloc(argc + def_argc + 3, sizeof(s_argv));
	assert(s_argv != NULL);
	s_argv[0] = "smapd";
	s_argc = 1;
	
	c_argv = calloc(argc + 3, sizeof(c_argv));
	assert(c_argv != NULL);
	c_argv[0] = "smapc";
	c_argc = 1;
	
	while ((i = getopt(argc, argv, "C:c:S:t:x:hv")) != EOF) {
		switch (i) {
		case 'c':
			config_file = optarg;
			break;

		case 'S':
			s_argv[s_argc++] = optarg;
			break;

		case 'C':
			c_argv[c_argc++] = optarg;
			break;

		case 't':
			if ((timeout = atoi(optarg)) <= 0) {
				error(EX_USAGE, 0, "bad timeout");
			}
			break;
			
		case 'x':
			extra_server = optarg;
			break;
			
		case 'h':
			usage(stdout);
			exit(0);

		case 'v':
			verbose++;
			break;
			
		default:
			exit(EX_USAGE);
		}
	}

	if (argc == optind) {
		usage(stderr);
		exit(EX_USAGE);
	}
	c_argv[c_argc++] = "-S";
	c_argv[c_argc++] = urlbuf;
	c_argv[c_argc++] = "-aq";
	for (i = optind; i < argc; i++)
		c_argv[c_argc++] = argv[i];
	c_argv[c_argc] = NULL;
	
	for (i = 0; i < def_argc; i++)
		s_argv[s_argc++] = def_args[i];

	if (config_file) {
		s_argv[s_argc++] = "-c";
		s_argv[s_argc++] = config_file;
	}
	s_argv[s_argc] = NULL;

	for (i = 0; i < sizeof(signum)/sizeof(signum[0]); i++)
		signal(signum[i], sighan);
	
	if (extra_server) {
		extra_pid = fork();
		if (extra_pid == -1)
			error(EX_OSERR, errno, "fork");
		if (extra_pid == 0) {
			char *xargv[4];
			int fd;
			
			if (verbose)
				printf("%s: starting extra server %s\n",
				       progname, extra_server);
			
			fd = open("/dev/null", O_RDWR);
			if (fd == -1)
				error(EX_OSERR, errno, "/dev/null");
			dup2(fd, 0);			
			dup2(fd, 1);

			fd = open("extra.log", O_CREAT|O_TRUNC|O_WRONLY, 0644);
			if (fd == -1)
				error(EX_OSERR, errno, "extra.log");
			dup2(fd, 2);
			close(fd);

			xargv[0] = getenv("SHELL");
			if (!xargv[0])
				xargv[0] = "/bin/sh";
			xargv[1] = "-c";
			xargv[2] = extra_server;
			xargv[3] = NULL;

			execvp(xargv[0], xargv);
			error(EX_OSERR, errno, "error running %s", extra_server);
			_exit(127);
		} else if (verbose)
			printf("%s: extra server %d\n", progname, extra_pid);
	}	
	
	lfd = listener_setup();
	server_pid = fork();
	if (server_pid == -1) {
		int ec = errno;
		if (extra_pid > 0)
			kill(extra_pid, SIGKILL);
		error(EX_OSERR, ec, "fork");
	}
	
	if (server_pid == 0) {
		/* Run server */
		int fd;
		struct sockaddr_in sin;
		socklen_t len = sizeof sin;
      
		if (verbose) {
			printf("%s: starting smap server", progname);
			for (i = 0; s_argv[i]; i++)
				printf(" %s", s_argv[i]);
			putchar('\n');
		}

		fd = accept(lfd, (struct sockaddr *)&sin, &len);

		if (fd == -1)
			error(EX_OSERR, errno, "accept");

		dup2(fd, 0);
		dup2(fd, 1);
		if (fd > 1)
			close(fd);
		close(lfd);

		execvp(s_argv[0], s_argv);
		error(EX_OSERR, errno, "error running %s", s_argv[0]);
		_exit(127);
	} else if (verbose)
		printf("%s: smap server %d\n", progname, server_pid);

	client_pid = fork();
	if (client_pid == -1) {
		int ec = errno;
		if (client_pid != -1)
			kill(client_pid, SIGKILL);
		kill(server_pid, SIGKILL);
		error(EX_OSERR, ec, "fork");
	}

	if (client_pid == 0) {
		if (verbose) {
			printf("%s: starting smap client", progname);
			for (i = 0; c_argv[i]; i++)
				printf(" %s", c_argv[i]);
			putchar('\n');
		}
		execvp(c_argv[0], c_argv);
		perror(c_argv[0]);
		_exit(127);
	} else if (verbose)
		printf("%s: smap client %d\n", progname, client_pid);

	/* Master */
	alarm(timeout);
	stop = 0;
	while (!stop) {
		switch (action) {
		case ACTION_CONT:
			pause();
			break;
			
		case ACTION_CLEANUP: {
			pid_t pid;
			int status;
			while ((pid = waitpid(-1, &status, WNOHANG)) > 0) {
				if (pid == client_pid) {
					progexited("smap client", status);
					stop = 1;
					client_pid = -1;
					if (WIFEXITED(status))
						result = WEXITSTATUS(status);
					else 
						result = EX_UNAVAILABLE;
				} else if (pid == server_pid) {
					progexited("smap server", status);
					server_pid = -1;
					result = EX_UNAVAILABLE;
				} else if (pid == extra_pid) {
					progexited("extra server", status);
					extra_pid = -1;
					result = EX_UNAVAILABLE;
				} else {
					error(0, 0,
					      "unrecogized child pid %d exited",
					      pid);
					break;
				}
				action = ACTION_CONT;
			}
			break;
		}

		case ACTION_TIMEOUT:
			error(0, 0, "timed out");
			result = EX_TEMPFAIL;
			stop = 1;
			break;

		case ACTION_CANCEL:
			error(0, 0, "cancelled");
			result = EX_UNAVAILABLE;
			stop = 1;
			break;
		}
	}

	if (client_pid > 0) 
		kill(client_pid, SIGKILL);
	if (server_pid > 0)
		kill(server_pid, SIGKILL);	
	if (extra_pid > 0)
		kill(extra_pid, SIGKILL);

	return result;
}
