/* This file is part of Smap.
   Copyright (C) 2006-2021 Sergey Poznyakoff

   Smap is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Smap is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Smap.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "smap/diag.h"
#include "smap/stream.h"
#include "smap/streamdef.h"

static int
null_write(struct _smap_stream *stream, const char *buf,
	   size_t size, size_t *pret)
{
	*pret = size;
	return 0;
}

static int
null_read(struct _smap_stream *stream, char *buf,
	  size_t size, size_t *pret)
{
	*pret = size;
	return 0;
}

int
smap_null_stream_create(smap_stream_t *pstream)
{
	smap_stream_t str =
		_smap_stream_create(sizeof(*str),
				    SMAP_STREAM_READ|SMAP_STREAM_WRITE);
	if (!str)
		return ENOMEM;
	str->read = null_read;
	str->write = null_write;
	*pstream = str;
	return 0;
}
