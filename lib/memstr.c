/* This file is part of Smap.
   Copyright (C) 2006-2021 Sergey Poznyakoff

   Smap is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Smap is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Smap.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "smap/diag.h"
#include "smap/stream.h"
#include "smap/streamdef.h"

struct memstream {
	struct _smap_stream base;
	char *ptr;
	size_t size;
	smap_off_t offset;
	size_t capacity;
};

#define BLOCKSIZE 4096

static int
mem_write(struct _smap_stream *stream, const char *buf,
	  size_t size, size_t *pret)
{
	struct memstream *mstr = (struct memstream *)stream;
  
	if (mstr->capacity < mstr->offset + size) {
		/* Realloc by fixed blocks of BLOCKSIZE. */
		size_t newsize =
			BLOCKSIZE * (((mstr->offset + size) / BLOCKSIZE) + 1);
		char *tmp = realloc(mstr->ptr, newsize);
		if (tmp == NULL)
			return ENOMEM;
		mstr->ptr = tmp;
		mstr->capacity = newsize;
	}

	memcpy(mstr->ptr + mstr->offset, buf, size);
  
	mstr->offset += size;

	if (mstr->offset > mstr->size)
		mstr->size = mstr->offset;
	
	*pret = size;
	return 0;
}

static int
mem_read(struct _smap_stream *stream, char *buf,
	 size_t size, size_t *pret)
{
	struct memstream *mstr = (struct memstream *)stream;
	size_t n = 0;
	if (mstr->ptr != NULL && ((size_t)mstr->offset <= mstr->size)) {
		n = ((mstr->offset + size) > mstr->size) ?
			mstr->size - mstr->offset : size;
		memcpy(buf, mstr->ptr + mstr->offset, n);
		mstr->offset += n;
	}
	*pret = n;
	return 0;
}

static int
mem_seek(struct _smap_stream *stream, smap_off_t off, smap_off_t *presult)
{ 
	struct memstream *mstr = (struct memstream *)stream;

	if (off < 0)
		return ESPIPE;
	mstr->offset = off;
	*presult = off;
	return 0;
}

static int
mem_truncate(struct _smap_stream *stream, smap_off_t len)
{
	struct memstream *mstr = (struct memstream *)stream;

	if (len > (smap_off_t) mstr->size) {
		char *tmp = realloc(mstr->ptr, len);
		if (tmp == NULL)
			return ENOMEM;
		mstr->ptr = tmp;
		mstr->capacity = len;
	}
	mstr->size = len;
	if (mstr->offset > mstr->size)
		mstr->offset = mstr->size;
	return 0;
}

static int
mem_size(struct _smap_stream *stream, smap_off_t *psize)
{
	struct memstream *mstr = (struct memstream *)stream;
	*psize = mstr->size;
	return 0;
}

static void
mem_done(struct _smap_stream *stream)
{
	struct memstream *mstr = (struct memstream *)stream;
	if (mstr && mstr->ptr != NULL)
		free(mstr->ptr);
}

static int
mem_close(struct _smap_stream *stream)
{
	struct memstream *mstr = (struct memstream *)stream;

	if (mstr->ptr)
		free (mstr->ptr);
	mstr->ptr = NULL;
	mstr->size = 0;
	mstr->capacity = 0;
	return 0;
}

int
smap_memory_stream_create(smap_stream_t *pstream)
{
	struct memstream *str = (struct memstream *)
		_smap_stream_create(sizeof(*str),
				    SMAP_STREAM_RDWR|SMAP_STREAM_SEEK);
	if (!str)
		return ENOMEM;
	str->base.read = mem_read;
	str->base.write = mem_write;
	str->base.close = mem_close;
	str->base.done = mem_done;
	str->base.seek = mem_seek;
	str->base.size = mem_size;
	str->base.truncate = mem_truncate;
	
	*pstream = (smap_stream_t)str;
	return 0;
}
