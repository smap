/* This file is part of Smap.
   Copyright (C) 2006-2021 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <stdlib.h>
#include <string.h>
#include <smap/stream.h>
#include <smap/diag.h>
#include <smap/module.h>
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

struct luadb {
	const char *dbid;
	lua_State *state;
	int start;
	int lookup;
	int xform;
};

static void *
mod_lua_alloc(void *ud, void *ptr, size_t osize, size_t nsize)
{
	if (nsize == 0) {
		free(ptr);
		return NULL;
	}
	return realloc(ptr, nsize);
}

static int
mod_lua_panic(lua_State *st)
{
	const char *msg = lua_tostring(st, -1);
	if (!msg)
		msg = "error object is not a string";
	smap_error("PANIC: unprotected error in call to Lua API (%s)", msg);
		   
	return 0;  /* FIXME: jump instead? */
}

static void
luadb_pcall_error(struct luadb *ldb, int status)
{
	switch (status) {
	case LUA_OK:
		break;
		
	case LUA_ERRMEM:
                smap_error("Lua ran out of memory");
                return;

	case LUA_ERRRUN:
		smap_error("Lua runtime error: %s",
			   lua_tostring(ldb->state, -1));
		break;
		
	case LUA_ERRERR:
		smap_error("Lua message handler error: %s",
			   lua_tostring(ldb->state, -1));
		break;
		
	case LUA_ERRGCMM:
                smap_error("Lua garbage collector error: %s",
			   lua_tostring(ldb->state, -1));
		break;
		
	default:
                smap_error("Lua unknonwn error: %s",
			   lua_tostring(ldb->state, -1));
        }
	lua_pop(ldb->state, 1);
}

static int
luadb_load(struct luadb *ldb, char const *filename)
{
	int n = lua_gettop(ldb->state);

	if (luaL_loadfile(ldb->state, filename)) {
		smap_error("error in Lua file '%s': %s", filename,
			   lua_tostring(ldb->state, -1));
		lua_pop(ldb->state, 1);
		return -1;
	}

	switch (lua_pcall(ldb->state, 0, LUA_MULTRET, 0)) {
	case LUA_OK:
		return lua_gettop(ldb->state) - n;
		
	case LUA_ERRMEM:
                smap_error("Lua ran out of memory");
                return -1;

	case LUA_ERRRUN:
		smap_error("Lua runtime error: %s",
			   lua_tostring(ldb->state, -1));
		break;
		
	case LUA_ERRERR:
		smap_error("Lua message handler error: %s",
			   lua_tostring(ldb->state, -1));
		break;
		
	case LUA_ERRGCMM:
                smap_error("Lua garbage collector error: %s",
			   lua_tostring(ldb->state, -1));
		break;
		
	default:
                smap_error("Lua unknonwn error: %s",
			   lua_tostring(ldb->state, -1));
        }
	lua_pop(ldb->state, 1);
	return -1;
}

static int
mod_free_db(smap_database_t dbp)
{
	struct luadb *ldb = (struct luadb *) dbp;
	lua_close(ldb->state);
	free(ldb);
	return 0;
}


static smap_database_t
mod_init_db(const char *dbid, int argc, char **argv)
{
	struct luadb *ldb;
	lua_State *state;
	int nret, n;
	
	if (argc != 2) {
		smap_error("%s: bad number of arguments", dbid);
		return NULL;
	}

	state = lua_newstate(mod_lua_alloc, NULL);
	if (!state) {
		smap_error("%s: not enough memory", dbid);
		return NULL;
	}
		
	lua_atpanic(state, &mod_lua_panic);

	luaL_openlibs(state);
	
	ldb = calloc(1, sizeof(*ldb));
	if (!ldb) {
		smap_error("%s: not enough memory", dbid);
		return NULL;
	}

	ldb->state = state;
	if (!(ldb->dbid = strdup(dbid))) {
		smap_error("%s: not enough memory", dbid);
		mod_free_db((smap_database_t) ldb);
		return NULL;
	}

	nret = luadb_load(ldb, argv[1]);
		
	if (nret == -1) {
		mod_free_db((smap_database_t) ldb);
		return NULL;
	}
	if (nret == 0) {
		smap_error("%s: %s returned no values", dbid, argv[1]);
		mod_free_db((smap_database_t) ldb);
		return NULL;
	}

	n = lua_gettop(ldb->state);
	ldb->start = n;
	if (lua_istable(ldb->state, -1)) {
		int t;
		
		lua_pushliteral(ldb->state, "lookup");
		t = lua_gettable(ldb->state, n);
		if (t == LUA_TFUNCTION)
			ldb->lookup = lua_gettop(ldb->state);
		else
			lua_pop(ldb->state, 1);
		
		lua_pushliteral(ldb->state, "xform");
		t = lua_gettable(ldb->state, n);
		if (t == LUA_TFUNCTION)
			ldb->xform = lua_gettop(ldb->state);
		else
			lua_pop(ldb->state, 1);
	} else {
		smap_error("%s: first value returned by %s is not a table",
			   dbid, argv[1]);
		mod_free_db((smap_database_t) ldb);
		ldb = NULL;
	} 

	return (smap_database_t) ldb;
}

static char default_negative_reply[] = "NOTFOUND";

static int
mod_query(smap_database_t dbp,
	  smap_stream_t ostr,
	  const char *map, const char *key,
	  struct smap_conninfo const *conninfo)
{
	struct luadb *ldb = (struct luadb *) dbp;

	if (ldb->lookup > 0) {
		int res;
		
		lua_pushvalue(ldb->state, ldb->lookup);
		lua_pushstring(ldb->state, map);
		lua_pushstring(ldb->state, key);
		res = lua_pcall(ldb->state, 2, 1, 0);
		if (res != LUA_OK) {
			luadb_pcall_error(ldb, res);
		} else {
			size_t len;
			char const *res = lua_tolstring(ldb->state, -1, &len);
			if (!res) {
				res = default_negative_reply;
				len = strlen(default_negative_reply);
			}
			smap_stream_write(ostr, res, len, NULL);
			smap_stream_write(ostr, "\n", 1, NULL);
			lua_pop(ldb->state, 1);
			return 0;
		}
	} else {
		smap_error("%s: lookup function not defined", ldb->dbid);
	}
	return 1;
}

static int
mod_xform(smap_database_t dbp,
	  struct smap_conninfo const *conninfo,
	  const char *input,
	  char **output)
{
	struct luadb *ldb = (struct luadb *) dbp;

	if (ldb->xform > 0) {
		int res;
		
		lua_pushvalue(ldb->state, ldb->xform);
		lua_pushstring(ldb->state, input);
		res = lua_pcall(ldb->state, 1, 1, 0);
		if (res != LUA_OK) {
			luadb_pcall_error(ldb, res);
		} else {
			size_t len;
			char const *res = lua_tolstring(ldb->state, -1, &len);
			char *p;
			if (!res) {
				res = input;
				len = strlen(res);
			}
			p = malloc(len + 1);
			if (!p) {
				smap_error("%s: out of memory", ldb->dbid);
				return 1;
			}
			memcpy(p, res, len);
			p[len] = 0;
			lua_pop(ldb->state, 1);

			*output = p;
			return 0;
		}
	} else {
		smap_error("%s: xform function not defined", ldb->dbid);
	}
	return 1;
}

static int
mod_dbcap(smap_database_t dbp)
{
	struct luadb *ldb = (struct luadb *) dbp;
	return ((ldb->lookup > 0) ? SMAP_CAPA_QUERY : 0) |
		((ldb->xform > 0) ? SMAP_CAPA_XFORM : 0);
} 

struct smap_module smap_module = {
	.smap_version = SMAP_MODULE_VERSION,
	.smap_capabilities = SMAP_CAPA_QUERY | SMAP_CAPA_XFORM | SMAP_CAPA_DBCAP,
	.smap_init_db = mod_init_db,
	.smap_free_db = mod_free_db,
	.smap_query = mod_query,
	.smap_xform = mod_xform,
	.smap_dbcap = mod_dbcap
};
