/* This file is part of Smap.
   Copyright (C) 2014-2021 Sergey Poznyakoff

   Smap is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Smap is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Smap.  If not, see <http://www.gnu.org/licenses/>. */

/*
 * This module implements a getent family of databases, which look up
 * data in the system databases (passwd, group, etc.)
 *
 *   module getent getent.so
 *
 *   database DBNAME getent pwnam|pwuid COMMON_OPTIONS
 *   database DBNAME getent grnam|grgid COMMON_OPTIONS
 *   database DBNAME getent groupmember NAMELIST COMMON_OPTIONS
 *   database DBNAME getent netgroup NAMELIST COMMON_OPTIONS
 *     (key: user@host.domain)
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include <smap/stream.h>
#include <smap/diag.h>
#include <smap/module.h>
#include <smap/parseopt.h>
#include <smap/wordsplit.h>

static char default_positive_reply[] = "OK";
static char default_negative_reply[] = "NOTFOUND";
static char default_onerror_reply[] = "PERM";

struct getent_database;
struct getvar_defn;

/*
 * This structure describes a particular sub-database.
 */
struct getent_func {
	char const *name;      /* Sub-db name */

	/* Additional size to be allocated at the end of struct
	   getent_database: */
	size_t extra_size;

	/* Parse command line and initialize the database instance. */
	int (*init_db)(struct getent_database *, int, char**);

	/* Reclaim allocated resources. */
	void (*free_db)(struct getent_database *);

	/* Reclaim memory allocated for a result data */
	void (*free_data)(void *);

	/* Template variable definitions. */
	struct getvar_defn *defn;

	/* Query function. */
	int (*query)(struct getent_database *, const char *, void **);
};

/*
 * Stub getent database structure.  Sub databases can expand it by
 * allocating additional space after it.
 */
struct getent_database {
	struct getent_func *func;
	char *positive_reply;
	char *negative_reply;
	char *onerror_reply;
};

/* Variable type handler */
typedef int (*GETENT_TYPE)(char **, void *);

/* Template variable definition. */
struct getvar_defn {
	const char *name; /* Variable name */
	size_t len;       /* Length of name */
	size_t off;       /* Offset of the data portion in the result
			     structure. */
	GETENT_TYPE type; /* Variable type. */
};

/* Convenience macro for defining first two members of struct getvar_defn */
#define VARNAME(s) #s, sizeof(#s)-1

/*
 * Variable type handlers.
 *
 * A handler is defined as
 *
 *    int type(char **ret, void *data);
 *
 * On entry, data contains a pointer to the actual data, and ret is a
 * memory location for the resulting expansion.
 *
 * The handler converts data to a printable representation (char*),
 * allocating the necessary memory via malloc.  On success, it stores
 * the pointer to the allocated memory in *ret.
 *
 * Returns a wordsplit error code indicating the status of the operation.
 */

/*
 * String variable
 */
static int
getent_type_string(char **ret, void *data)
{
	char *p = strdup(*(char**)data);
	if (!p)
		return WRDSE_NOSPACE;
	*ret = p;
	return WRDSE_OK;
}

/*
 * A list of strings.  Data points to a char **.
 * The result is formatted as a comma-delimited list of strings.
 */
static int
getent_type_stringlist(char **ret, void *data)
{
	char **p = *(char***)data;
	size_t len;
	int i;
	char *buf, *q;

	len = 1;
	for (i = 0; p[i]; i++)
		len += strlen(p[i]);
	len += i;

	buf = malloc(len);
	if (!buf)
		return WRDSE_NOSPACE;
	*ret = buf;

	for (i = 0; p[i]; i++) {
		if (i)
			*buf++ = ',';
		for (q = p[i]; *q;)
			*buf++ = *q++;
	}
	*buf = 0;

	return WRDSE_OK;
}

/*
 * Same as above, but data points to a char *X[].
 */
static int
getent_type_stringarray(char **ret, void *data)
{
	char **p = (char**)data;
	size_t len;
	int i;
	char *buf, *q;

	len = 1;
	for (i = 0; p[i]; i++)
		len += strlen(p[i]);
	len += i;

	buf = malloc(len);
	if (!buf)
		return WRDSE_NOSPACE;
	*ret = buf;

	for (i = 0; p[i]; i++) {
		if (i)
			*buf++ = ',';
		for (q = p[i]; *q;)
			*buf++ = *q++;
	}
	*buf = 0;

	return WRDSE_OK;
}

/*
 * Auxiliary macros for formatting arbitrary integer numbers.
 * Taken from gnulib (GPLv3+).
 * Originally written by: Paul Eggert.
 */
#define TYPE_WIDTH(t) (sizeof (t) * CHAR_BIT)
/* Bound on length of the string representing an unsigned integer
      value representable in B bits.  log10 (2.0) < 146/485.  The
      smallest value of B where this bound is not tight is 2621.  */
#define INT_BITS_STRLEN_BOUND(b) (((b) * 146 + 484) / 485)
#define INT_STRLEN_BOUND(t)                                     \
	(INT_BITS_STRLEN_BOUND (TYPE_WIDTH (t) - 1) + 1)
#define INT_BUFSIZE_BOUND(t) (INT_STRLEN_BOUND (t) + 1)

#define ANYTOSTR(inttype, i)			\
	char buf[INT_BUFSIZE_BOUND(inttype)];		\
	char *p = buf + INT_STRLEN_BOUND(inttype);	\
	*p = 0;						\
	if (i < 0) {					\
		do                                      \
			*--p = '0' - i % 10;            \
		while ((i /= 10) != 0);                 \
		*--p = '-';                             \
	} else {                                        \
		do                                      \
			*--p = '0' + i % 10;            \
		while ((i /= 10) != 0);                 \
	}                                               \

/* Format a uid_t */
static int
getent_type_uid(char **ret, void *data)
{
	uid_t uid = *(uid_t*)data;
	ANYTOSTR(uid_t, uid);
	*ret = strdup(p);
	if (!*ret)
		return WRDSE_NOSPACE;
	return WRDSE_OK;
}

/* Format a gid_t */
static int
getent_type_gid(char **ret, void *data)
{
	uid_t gid = *(gid_t*)data;
	ANYTOSTR(gid_t, gid);
	*ret = strdup(p);
	if (!*ret)
		return WRDSE_NOSPACE;
	return WRDSE_OK;
}

/*
 * Default database initialization function.  It handles the
 * options supported by all sub-databases in this module (referred
 * in the comments below as COMMON_OPTIONS):
 *   positive-reply=STRING
 *   negative-reply=STRING
 *   onerror-reply=STRING.
 */
static int
default_init_db(struct getent_database *db, int argc, char **argv)
{
	struct smap_option init_option[] = {
		{ SMAP_OPTSTR(positive-reply), smap_opt_string,
		  &db->positive_reply },
		{ SMAP_OPTSTR(negative-reply), smap_opt_string,
		  &db->negative_reply },
		{ SMAP_OPTSTR(onerror-reply), smap_opt_string,
		  &db->onerror_reply },
		{ NULL }
	};

	return smap_parseopt(init_option, argc, argv, 0, NULL);
}

/*
 * pwnam and pwuid databases:
 *
 * database DBNAME getent pwnam|pwuid COMMON_OPTIONS
 */

/* Result is returned as an objet of this type: */
struct getpw_buf {
	struct passwd pw;
	char *buf;
	size_t buflen;
};

/* Free the memory allocated for struct getpw_buf. */
static void
getpw_buf_free(void *data)
{
	struct getpw_buf *p = data;
	free(p->buf);
	free(p);
}

/* Variable definitions for members of struct password */
static struct getvar_defn getpw_defn[] = {
	{ VARNAME(name),
	  offsetof(struct getpw_buf, pw.pw_name),
	  getent_type_string },
	{ VARNAME(passwd),
	  offsetof(struct getpw_buf, pw.pw_passwd),
	  getent_type_string },
	{ VARNAME(uid),
	  offsetof(struct getpw_buf, pw.pw_uid),
	  getent_type_uid },
	{ VARNAME(gid),
	  offsetof(struct getpw_buf, pw.pw_gid),
	  getent_type_gid },
	{ VARNAME(gecos),
	  offsetof(struct getpw_buf, pw.pw_gecos),
	  getent_type_string },
	{ VARNAME(dir),
	  offsetof(struct getpw_buf, pw.pw_dir),
	  getent_type_string },
	{ VARNAME(shell),
	  offsetof(struct getpw_buf, pw.pw_shell),
	  getent_type_string },
	{ NULL }
};

/*
 * getpwnam: key points to a user name.
 */
static int
getpwnam_query(struct getent_database *db, const char *key, void **ret)
{
	struct passwd *pw;
	struct getpw_buf *pwbuf;

	pwbuf = calloc(1, sizeof(*pwbuf));
	if (!pwbuf)
		return -1;
	pwbuf->buflen = sysconf(_SC_GETPW_R_SIZE_MAX);
	pwbuf->buf = malloc(pwbuf->buflen);
	if (!pwbuf->buf) {
		getpw_buf_free(pwbuf);
		errno = ENOMEM;
		return -1;
	}
	while (getpwnam_r(key, &pwbuf->pw, pwbuf->buf, pwbuf->buflen, &pw)) {
		if (errno == ERANGE) {
			if ((size_t) -1 / 3 * 2 <= pwbuf->buflen) {
				getpw_buf_free(pwbuf);
				errno = ENOMEM;
				return -1;
			} else {
				char *p;
				pwbuf->buflen += (pwbuf->buflen + 1) / 2;
				p = realloc(pwbuf->buf, pwbuf->buflen);
				if (!p) {
					getpw_buf_free(pwbuf);
					errno = ENOMEM;
					return -1;
				}
				pwbuf->buf = p;
			}
		} else
			return -1;
	}

	if (pw) {
		*ret = pwbuf;
	} else {
		getpw_buf_free(pwbuf);
		*ret = NULL;
	}

	return 0;
}

/*
 * getpwuid: key points to a UID formatted as ASCII string.
 */
static int
getpwuid_query(struct getent_database *db, const char *key, void **ret)
{
	struct passwd *pw;
	struct getpw_buf *pwbuf;
	uid_t uid;
	int sign;

	while (*key && (*key == ' ' || *key == '\t'))
		key++;
	if (key[0] == '-' || key[0] == '+') {
		sign = key[0] == '-';
		key++;
		while (*key && (*key == ' ' || *key == '\t'))
			key++;
	} else
		sign = 0;

	if (!*key) {
		errno = EINVAL;
		return -1;
	}

	uid = 0;
	while (*key) {
		int n = *key++ - '0';
		if (n >= 0 && n < 10) {
			uid = uid * 10 + n;
		} else {
			errno = EINVAL;
			return -1;
		}
	}

	if (sign)
		uid = - uid;

	pwbuf = calloc(1, sizeof(*pwbuf));
	if (!pwbuf)
		return -1;
	pwbuf->buflen = sysconf(_SC_GETPW_R_SIZE_MAX);
	pwbuf->buf = malloc(pwbuf->buflen);
	if (!pwbuf->buf) {
		getpw_buf_free(pwbuf);
		errno = ENOMEM;
		return -1;
	}
	while (getpwuid_r(uid, &pwbuf->pw, pwbuf->buf, pwbuf->buflen, &pw)) {
		if (errno == ERANGE) {
			if ((size_t) -1 / 3 * 2 <= pwbuf->buflen) {
				getpw_buf_free(pwbuf);
				errno = ENOMEM;
				return -1;
			} else {
				char *p;
				pwbuf->buflen += (pwbuf->buflen + 1) / 2;
				p = realloc(pwbuf->buf, pwbuf->buflen);
				if (!p) {
					getpw_buf_free(pwbuf);
					errno = ENOMEM;
					return -1;
				}
				pwbuf->buf = p;
			}
		} else
			return -1;
	}

	if (pw) {
		*ret = pwbuf;
	} else {
		getpw_buf_free(pwbuf);
		*ret = NULL;
	}

	return 0;
}

/*
 * grnam and gruid databases:
 *
 * database DBNAME getent grnam|gruid COMMON_OPTIONS
 */

/* Result structure */
struct getgr_buf {
	struct group gr;
	char *buf;
	size_t buflen;
};

/* Free memory allocated for the result */
static void
getgr_buf_free(void *data)
{
	struct getgr_buf *p = data;
	free(p->buf);
	free(p);
}

/* Variable definitions for members of struct group */
static struct getvar_defn getgr_defn[] = {
	{ VARNAME(name),
	  offsetof(struct getgr_buf, gr.gr_name),
	  getent_type_string },
	{ VARNAME(passwd),
	  offsetof(struct getgr_buf, gr.gr_passwd),
	  getent_type_string },
	{ VARNAME(gid),
	  offsetof(struct getgr_buf, gr.gr_gid),
	  getent_type_gid },
	{ VARNAME(mem),
	  offsetof(struct getgr_buf, gr.gr_mem),
	  getent_type_stringlist },
	{ NULL }
};


/*
 * getgrnam: key points to the group name.
 */
static int
getgrnam_query(struct getent_database *db, const char *key, void **ret)
{
	struct group *gr;
	struct getgr_buf *grbuf;

	grbuf = calloc(1, sizeof(*grbuf));
	if (!grbuf)
		return -1;
	grbuf->buflen = sysconf(_SC_GETGR_R_SIZE_MAX);
	grbuf->buf = malloc(grbuf->buflen);
	if (!grbuf->buf) {
		getgr_buf_free(grbuf);
		errno = ENOMEM;
		return -1;
	}
	while (getgrnam_r(key, &grbuf->gr, grbuf->buf, grbuf->buflen, &gr)) {
		if (errno == ERANGE) {
			if ((size_t) -1 / 3 * 2 <= grbuf->buflen) {
				getgr_buf_free(grbuf);
				errno = ENOMEM;
				return -1;
			} else {
				char *p;
				grbuf->buflen += (grbuf->buflen + 1) / 2;
				p = realloc(grbuf->buf, grbuf->buflen);
				if (!p) {
					getgr_buf_free(grbuf);
					errno = ENOMEM;
					return -1;
				}
				grbuf->buf = p;
			}
		} else
			return -1;
	}

	if (gr) {
		*ret = grbuf;
	} else {
		getgr_buf_free(grbuf);
		*ret = NULL;
	}

	return 0;
}

/*
 * getgrgid: key points to the group ID, formatted as ASCII string.
 */
static int
getgrgid_query(struct getent_database *db, const char *key, void **ret)
{
	struct group *gr;
	struct getgr_buf *grbuf;
	gid_t gid;
	int sign;

	while (*key && (*key == ' ' || *key == '\t'))
		key++;
	if (key[0] == '-' || key[0] == '+') {
		sign = key[0] == '-';
		key++;
		while (*key && (*key == ' ' || *key == '\t'))
			key++;
	} else
		sign = 0;

	if (!*key) {
		errno = EINVAL;
		return -1;
	}

	gid = 0;
	while (*key) {
		int n = *key++ - '0';
		if (n >= 0 && n < 10) {
			gid = gid * 10 + n;
		} else {
			errno = EINVAL;
			return -1;
		}
	}

	if (sign)
		gid = - gid;

	grbuf = calloc(1, sizeof(*grbuf));
	if (!grbuf)
		return -1;
	grbuf->buflen = sysconf(_SC_GETGR_R_SIZE_MAX);
	grbuf->buf = malloc(grbuf->buflen);
	if (!grbuf->buf) {
		getgr_buf_free(grbuf);
		errno = ENOMEM;
		return -1;
	}
	while (getgrgid_r(gid, &grbuf->gr, grbuf->buf, grbuf->buflen, &gr)) {
		if (errno == ERANGE) {
			if ((size_t) -1 / 3 * 2 <= grbuf->buflen) {
				getgr_buf_free(grbuf);
				errno = ENOMEM;
				return -1;
			} else {
				char *p;
				grbuf->buflen += (grbuf->buflen + 1) / 2;
				p = realloc(grbuf->buf, grbuf->buflen);
				if (!p) {
					getgr_buf_free(grbuf);
					errno = ENOMEM;
					return -1;
				}
				grbuf->buf = p;
			}
		} else
			return -1;
	}

	if (gr) {
		*ret = grbuf;
	} else {
		getgr_buf_free(grbuf);
		*ret = NULL;
	}

	return 0;
}

/*
 * The groupmember database.
 *
 * It is defined as:
 *
 *   database DBNAME getent groupmember NAMELIST COMMON_OPTIONS
 *
 * where NAMELIST stands for one or more group names.  Numeric GID
 * can be given as part of NAMELIST if it begins with a plus sign.
 *
 * The groupmember database returns positive reply if the user
 * identified by key, is a member of at least one group listed in
 * NAMELIST.  In this case the variable "list" in the positive reply
 * is expanded to a comma-delimited list of group names (from the
 * NAMELIST) the user is member of.
 */

/* The groupmember_database expands struct getent_database. */
struct groupmember_database {
	struct getent_database getent; 
	size_t grc; /* Number of elements in grv. */
	char **grv; /* List of group names */
};

/* Initialize the groupmember database. */
static int
groupmember_init_db(struct getent_database *db, int argc, char **argv)
{
	struct groupmember_database *memdb = (struct groupmember_database *)db;
	int i;
	struct smap_option init_option[] = {
		{ SMAP_OPTSTR(positive-reply), smap_opt_string,
		  &memdb->getent.positive_reply },
		{ SMAP_OPTSTR(negative-reply), smap_opt_string,
		  &memdb->getent.negative_reply },
		{ SMAP_OPTSTR(onerror-reply), smap_opt_string,
		  &memdb->getent.onerror_reply },
		{ NULL }
	};

	if (smap_parseopt(init_option, argc, argv, SMAP_PARSEOPT_PERMUTE, &i))
		return -1;

	if (argc == i) {
		smap_error("required arguments missing");
		return -1;
	}

	memdb->grv = calloc(argc - i + 1, sizeof(memdb->grv[0]));
	if (!memdb->grv) {
		errno = ENOMEM;
		return -1;
	}

	memdb->grc = 0;
	for (; i < argc; i++) {
		char *grname;

		if (argv[i][0] == '+') {
			char *p;
			unsigned long n;
			struct group *gr;

			errno = 0;
			n = strtoul(argv[i], &p, 10);
			if (errno || *p) {
				smap_error("%s: invalid gid", argv[i]);
				continue;
			}
			if ((gr = getgrgid(n)) == NULL) {
				smap_error("%s: no such group", argv[i]);
				continue;
			}
			grname = gr->gr_name;
		} else
			grname = argv[i];
		if ((memdb->grv[memdb->grc++] = strdup(grname)) == NULL) {
			errno = ENOMEM;
			return -1;
		}
	}
	memdb->grv[memdb->grc] = NULL;

	return 0;
}

/*
 * Reclaim the memory allocated for db.
 * Note: members of struct getent_database and the db itself are
 * handled by the caller.
 */
static void
groupmember_free_db(struct getent_database *db)
{
	struct groupmember_database *memdb = (struct groupmember_database *)db;
	size_t i;

	for (i = 0; i < memdb->grc; i++)
		free(memdb->grv[i]);
	free(memdb->grv);
}

/* Result is returned in this structure: */
struct groupname_list {
	size_t grc;
	char const *grv[1];
};

static void
groupname_list_free(void *p)
{
	free(p);
}

/*
 * If name is listed in the grv[] array, return the corresponding
 * element from it.
 */
static char const *
groupmember_check(struct groupmember_database *db, char const *name)
{
	size_t i;
	for (i = 0; i < db->grc; i++)
		if (strcmp(name, db->grv[i]) == 0)
			return db->grv[i];
	return NULL;
}

/*
 * If name is listed in the gr_mem member of gr, return 0.  Otherwise,
 * return 1.
 */
static int
grpmem(struct group *gr, char const *name)
{
	int i;

	for (i = 0; gr->gr_mem[i]; i++)
		if (strcmp(gr->gr_mem[i], name) == 0)
			return 0;
	return 1;
}

static int
groupmember_query(struct getent_database *db, const char *key, void **ret)
{
	struct groupmember_database *memdb = (struct groupmember_database *)db;
	struct passwd *pw;
	struct group *gr;
	struct groupname_list *namelist;
	char const *name;

	pw = getpwnam(key);
	if (!pw) {
		*ret = NULL;
		return 0;
	}

	namelist = calloc(1, sizeof(*namelist) + memdb->grc * sizeof(namelist->grv[0]));
	if (!namelist) {
		errno = ENOMEM;
		return -1;
	}

	if ((gr = getgrgid(pw->pw_gid)) != NULL &&
	    (name = groupmember_check(memdb, gr->gr_name)) != NULL)
		namelist->grv[namelist->grc++] = name;

	setgrent();
	while ((gr = getgrent()) != NULL) {
		if (gr->gr_gid != pw->pw_gid &&
		    grpmem(gr, key) == 0 &&
		    (name = groupmember_check(memdb, gr->gr_name)) != NULL)
			namelist->grv[namelist->grc++] = name;
	}
	endgrent();

	if (namelist->grc == 0) {
		groupname_list_free(namelist);
		namelist = NULL;
	} else
		namelist->grv[namelist->grc] = NULL;

	*ret = namelist;
	return 0;
}

static struct getvar_defn groupmember_defn[] = {
	{ VARNAME(list),
	  offsetof(struct groupname_list, grv),
	  getent_type_stringarray },
	{ NULL }
};

/*
 * netgroup database.
 *
 * Declared as:
 *
 *   database DBNAME getent netgroup NAMELIST COMMON_OPTIONS
 *
 *  where NAMELIST stands for one or more netgroup names.
 *
 * The key is parsed as user@host.domain. Any of its parts can
 * be missing.
 *
 * The database returns positive result if the key matches one
 * or more netgroups from the list.  The variable "list" in the
 * positive reply is then expanded to a comma-delimited list of
 * matching netgroup names.
 */
static int
netgroup_query(struct getent_database *db, const char *key, void **ret)
{
	struct groupmember_database *memdb = (struct groupmember_database *)db;
	char *host, *user, *domain;
	char *buf;
	struct groupname_list *namelist;
	size_t i;
	
	buf = strdup(key);
	if (!buf)
		return -1;
	host = strchr(buf, '@');
	if (host) {
		user = buf;
		*host++ = 0;
	} else {
		user = NULL;
		host = buf;
	}

	domain = strchr(host, '.');
	if (domain)
		*domain++ = 0;

	namelist = calloc(1, sizeof(*namelist) + memdb->grc * sizeof(namelist->grv[0]));
	if (!namelist) {
		free(buf);
		errno = ENOMEM;
		return -1;
	}

	for (i = 0; i < memdb->grc; i++) {
		if (innetgr(memdb->grv[i], host, user, domain))
			namelist->grv[namelist->grc++] = memdb->grv[i];
	}
	free(buf);

	if (namelist->grc == 0) {
		groupname_list_free(namelist);
		namelist = NULL;
	} else
		namelist->grv[namelist->grc] = NULL;

	*ret = namelist;
	return 0;
}

/* Database definitions: */
static struct getent_func getent_func_tab[] = {
	{ .name = "pwnam",
	  .init_db = default_init_db,
	  .free_data = getpw_buf_free,
	  .defn = getpw_defn,
	  .query = getpwnam_query },
	{ .name = "pwuid",
	  .init_db = default_init_db,
	  .free_data = getpw_buf_free,
	  .defn = getpw_defn,
	  .query = getpwuid_query },
	{ .name = "grnam",
	  .init_db = default_init_db,
	  .free_data = getgr_buf_free,
	  .defn = getgr_defn,
	  .query = getgrnam_query },
	{ .name = "grgid",
	  .init_db = default_init_db,
	  .free_data = getgr_buf_free,
	  .defn = getgr_defn,
	  .query = getgrgid_query },
	{ .name = "groupmember",
	  .extra_size = sizeof(struct groupmember_database) - sizeof(struct getent_database),
	  .init_db = groupmember_init_db,
	  .free_db = groupmember_free_db,
	  .free_data = groupname_list_free,
	  .defn = groupmember_defn,
	  .query = groupmember_query },
	{ .name = "netgroup",
	  .extra_size = sizeof(struct groupmember_database) - sizeof(struct getent_database),
	  .init_db = groupmember_init_db,
	  .free_db = groupmember_free_db,
	  .free_data = groupname_list_free,
	  .defn = groupmember_defn,
	  .query = netgroup_query },
	{ NULL }
};

static void
getent_database_free(struct getent_database *db)
{
	if (db->func->free_db)
		db->func->free_db(db);
	if (db->positive_reply != default_positive_reply)
		free(db->positive_reply);
	if (db->negative_reply != default_negative_reply)
		free(db->negative_reply);
	if (db->onerror_reply != default_onerror_reply)
		free(db->onerror_reply);
	free(db);
}

static smap_database_t
getent_init_db(const char *dbid, int argc, char **argv)
{
	struct getent_func *pf;
	struct getent_database *db;

	if (argc < 2) {
		smap_error("missing database subtype");
		return NULL;
	}

	for (pf = getent_func_tab; pf->name; pf++)
		if (strcmp(argv[1], pf->name) == 0)
			break;
	if (!pf->name) {
		smap_error("%s: %s: unknown database subtype", dbid, argv[1]);
		return NULL;
	}

	db = calloc(1, sizeof(db[0]) + pf->extra_size);
	if (!db) {
		smap_error("%s: not enough memory", dbid);
		return NULL;
	}
	db->func = pf;
	db->positive_reply = default_positive_reply;
	db->negative_reply = default_negative_reply;
	db->onerror_reply = default_onerror_reply;

	if (pf->init_db(db, argc - 1, argv + 1)) {
		getent_database_free(db);
		return NULL;
	}

	return (smap_database_t) db;
}

static int
getent_free_db(smap_database_t dbp)
{
	struct getent_database *db = (struct getent_database *) dbp;
	getent_database_free(db);
	return 0;
}


struct getent_closure {
	struct getent_database *db;
	void *data;
};

static int
getent_getvar(char **ret, const char *var, size_t len, void *closure_data)
{
	struct getent_closure *clos = closure_data;
	struct getvar_defn *dfn = clos->db->func->defn;

	for (; dfn->name; dfn++) {
		if (len == dfn->len && memcmp(var, dfn->name, dfn->len) == 0)
			return dfn->type(ret, (char*)clos->data + dfn->off);
	}
	return WRDSE_UNDEF;
}

static int
getent_query(smap_database_t dbp,
	     smap_stream_t ostr,
	     const char *map, const char *key,
	     struct smap_conninfo const *conninfo)
{
	struct getent_database *db = (struct getent_database *) dbp;
	struct wordsplit ws;
	int rc;
	void *data = NULL;
	char *env[] = {
		"map",
		(char*)map,
		"key",
		(char*)key,
		NULL
	};
	struct getent_closure clos;
	int wsflags = WRDSF_NOSPLIT |
		      WRDSF_NOCMD |
		      WRDSF_ENV_KV |
		      WRDSF_ERROR |
		      WRDSF_SHOWERR;
	char *template;

	rc = db->func->query(db, key, &data);
	if (rc == 0) {
		if (data) {
			template = db->positive_reply;
			clos.db = db;
			clos.data = data;
			ws.ws_getvar = getent_getvar;
			ws.ws_closure = &clos;
			wsflags |= WRDSF_GETVAR | WRDSF_CLOSURE;
		} else {
			template = db->negative_reply;
		}
	} else {
		smap_error("%s %s: query failed: %s", map, key,
			   strerror(errno));
		template = db->onerror_reply;
	}

	ws.ws_env = (const char **) env;
	ws.ws_error = smap_error;
	rc = wordsplit(template, &ws, wsflags);
	if (rc)
		smap_error("%s %s: cannot format reply", map, key);
	else
		smap_stream_printf(ostr, "%s\n", ws.ws_wordv[0]);
	wordsplit_free(&ws);
	if (data)
		db->func->free_data(data);
	return rc;
}

struct smap_module smap_module = {
	.smap_version = SMAP_MODULE_VERSION,
	.smap_capabilities = SMAP_CAPA_DEFAULT,
	.smap_init_db = getent_init_db,
	.smap_free_db = getent_free_db,
	.smap_query = getent_query,
};
