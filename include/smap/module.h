/* This file is part of Smap.
   Copyright (C) 2010-2021 Sergey Poznyakoff

   Smap is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Smap is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Smap.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef __SMAP_MODULE_H
#define __SMAP_MODULE_H

#define SMAP_MODULE_VERSION 3
#define SMAP_CAPA_NONE 0
#define SMAP_CAPA_QUERY 0x0001 /* Module can perform queries */
#define SMAP_CAPA_XFORM 0x0002 /* Module can transform strings */
#define SMAP_CAPA_DBCAP 0x0004 /* Database capabilities can differ.
				  Call mod->smap_dbcap() to verify. */
#define SMAP_CAPA_DEFAULT SMAP_CAPA_QUERY

typedef struct smap_database *smap_database_t;

struct sockaddr;

struct smap_conninfo {
	struct sockaddr const *src;
	int srclen;
	struct sockaddr const *dst;
	int dstlen;
};

struct smap_module {
	unsigned smap_version;
	unsigned smap_capabilities;
	int (*smap_init)(int argc, char **argv);
	smap_database_t (*smap_init_db)(const char *dbid,
					int argc, char **argv);
	int (*smap_free_db)(smap_database_t dbp);
	int (*smap_open) (smap_database_t hp);
	int (*smap_close) (smap_database_t hp);
	int (*smap_query)(smap_database_t dbp,
			  smap_stream_t ostr,
			  const char *map, const char *key,
			  struct smap_conninfo const *conninfo);
	int (*smap_xform)(smap_database_t dbp,
			  struct smap_conninfo const *conninfo,
			  const char *input,
			  char **output);
	int (*smap_dbcap)(smap_database_t dbp);
};

#endif
